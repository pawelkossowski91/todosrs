import React, { Component } from "react";
import "./App.css";
// import "./style.css";
import List from "./List";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: "",
      items: []
    };
  }



  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-8 ">
            <h2><i>todo list app.</i></h2>
            <List />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
